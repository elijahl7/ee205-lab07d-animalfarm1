#pragma once

#include <stdio.h>
#include <stdbool.h>

#define BAD_CAT false
#define GOOD_CAT true

extern bool validateCatColorCombination(const unsigned long, const enum color, const enum color);

extern bool addCat(const char *, const enum gender, const enum breed, const bool, const float, const enum color, const enum color, const unsigned long long);