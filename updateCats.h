#pragma once

#include <stdbool.h>

extern bool fixCat(unsigned long);

extern bool updateCatName(unsigned long, char *);

extern bool updateCatWeight(unsigned long, float);

extern bool updateCatCollar1(const unsigned long, const enum color);

extern bool updateCatCollar2(const unsigned long, const enum color);

extern bool updateLicense(const unsigned long, const unsigned long long);