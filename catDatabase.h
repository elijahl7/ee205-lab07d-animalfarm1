#pragma once

#include <stdio.h>
#include <stdbool.h>

#define MAX_CATS 20
#define MAX_NAME_LENGTH 30

enum gender {
    UNKNOWN_GENDER,
    MALE,
    FEMALE
};

enum breed {
    UNKNOWN_BREED,
    MAINE_COON,
    MANX,
    SHORTHAIR,
    PERSIAN,
    SPHYNX
};

enum color {
    BLACK,
    WHITE,
    RED,
    BLUE,
    GREEN,
    PINK
};

struct Cat {
    char                name[MAX_NAME_LENGTH]   ;
    enum gender         gender                  ;
    enum breed          breed                   ;
    bool                isFixed                 ;
    float               weight                  ;
    enum color          collarColor1            ;
    enum color          collarColor2            ;
    unsigned long long  license                 ;
};

extern struct Cat cats[MAX_CATS];

extern size_t numCats;

extern size_t initDatabase();
