OBJS	= *.o
SOURCE	= *.c
HEADER	= *.h
OUT	= animalFarm1
CC	 = gcc
FLAGS	 = -g -c -Wall -Wextra

all: $(OBJS)
	$(CC) -g $(OBJS) -o $(OUT)

*.o: *.c
	$(CC) $(FLAGS) *.c 


clean:
	rm -f $(OBJS)

run:
	clear
	./$(OUT)

buildDebug:
	$(CC) $(FLAGS) *.c -D DEBUG

debug: buildDebug all run

test: all run